package Test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;

import org.apache.commons.lang3.math.NumberUtils;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

public class Reminder {

	String g_Input = "";
//	static String  reminder="";
//	static String requestid="";
//	static String status="No";
	
	static ArrayList<String> reminder=new ArrayList<String>();
	static ArrayList<String> requestid=new ArrayList<String>();
	static ArrayList<String> status=new ArrayList<String>();
	
	public static String validateRequestid(String input) {
  	  

		String result = null ;
 	   if(input.isEmpty()) {
 		   System.out.println("Empty/Null value is entered");
 		   result= "Request ID entered is Empty/Null. \n";
 	   }
 	   
 	   else {
	    	 	   if(input.length()>=9 && NumberUtils.isDigits(input)) {
	    			   System.out.println("Start execution for Requestid:"+input); 
	    			   result="Valid";
	    			}
	    		   else if(input.length()<9 || ! NumberUtils.isDigits(input)) {
	    			   System.out.println("Length is less than 9 or all are not digits for Request id:"+input);
	    			   result= "Request ID Entered = "+input+ " . Length is less than 9 or all are not digits.\n";
	    		   }
 	   }
 	   
 	   return result;  
 	   
    }

	public static void main(String[] args) throws Exception {


		JFrame jframe = new JFrame("Verification automation- Phone Number Search");
		jframe.setLayout(null);
		jframe.setSize(600, 400);
		jframe.setLocationRelativeTo(null);// center of the screen
		
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		

	     
		
		JLabel label = new JLabel("Input Request ID:-");
		label.setBounds(100, 10, 400, 30);
		
		JTextField textField = new JTextField();
		textField.setBounds(100, 40, 125, 30);
		
		JLabel Comment_label = new JLabel("User Comment:-");
		Comment_label.setBounds(100, 90, 400, 30);
		
		JTextField Comment_textField = new JTextField();
		Comment_textField.setBounds(100, 120, 400, 30);
		
		
		JLabel label2 = new JLabel("Select Date");
		label2.setBounds(100, 180, 400, 20);
		
		UtilDateModel model = new UtilDateModel();
		JDatePanelImpl datePanel = new JDatePanelImpl(model);
		JDatePickerImpl datePicker = new JDatePickerImpl(datePanel);
		datePicker.setBounds(100,210,200,30);
     
		JLabel label3 = new JLabel("Select Time");
		label3.setBounds(320, 180, 400, 20);
		
		Date date = new Date();
		SpinnerDateModel sm = new SpinnerDateModel(date, null, null, Calendar.MINUTE);
		JSpinner spinner = new JSpinner(sm);
		JSpinner.DateEditor de = new JSpinner.DateEditor(spinner, "HH:mm ");
		de.getTextField().setEditable( true );
		spinner.setEditor(de);
		spinner.setBounds(320,210,100,30); 

		
		JButton jbutton = new JButton("Submit");
		jbutton.setBounds(250, 280, 80, 40);
		jbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String input = textField.getText();
				//requestid=input;
				requestid.add(input);
				String temp = validateRequestid(input);
				
				if (temp!="Valid") {
					JOptionPane.showMessageDialog(null, temp+"Please enter correct Request Id and re-submit", "ErrorMsg", JOptionPane.ERROR_MESSAGE);
				}
				
				
				
			Date selectedDate = (Date) datePicker.getModel().getValue();
            String temp3=selectedDate.toString();
            System.out.println("Temp "+temp3);
            String temp1=de.getFormat().format(spinner.getValue());
            System.out.println("spinner "+temp1);
	        
	        String ipattern = "EEEEE MMMMM dd HH:mm:ss Z yyyy";
	        SimpleDateFormat opattern = new SimpleDateFormat("yyyy-MM-dd zzz");
	        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ipattern);
	        Date date1 = null;
		       
	        	try {
		            date1 = simpleDateFormat.parse(temp3);
		        } catch (ParseException e1) {
		            // TODO Auto-generated catch block
		            e1.printStackTrace();
		        }
		        
	        String formattedDate = opattern.format(date1);
	        String  outval="";
	        outval=formattedDate+ " "+temp1;
	        System.out.println(outval);
	        
	        //reminder=outval;
	        reminder.add(outval);
	        status.add("No");
	        
	        
			MyThreadClass tp = new MyThreadClass();
			tp.setName(input);
				if (MyThreadClass.runningThread != null) {
					MyThreadClass.bExit = true;
					while (MyThreadClass.runningThread.isAlive()) {
					}
					
					try {
						writeToFile();
						readFromFile();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			MyThreadClass.runningThread = tp;
			MyThreadClass.bExit = false;
			tp.start();
			
			try {
				writeToFile();
				readFromFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	

			}
		});

		//JLabel label1 = new JLabel("Results:-");
		//label1.setBounds(100, 260, 400, 20);

		//JTextArea textArea = new JTextArea();
		//textArea.setBounds(100, 280, 300, 250);
		
	

		jframe.add(label);
		jframe.add(textField);
		jframe.add(jbutton);
		jframe.add(Comment_label);
		jframe.add(Comment_textField);
		jframe.add(datePicker);
		jframe.add(spinner); 
		//jframe.add(label1);
		//jframe.add(textArea);
		jframe.add(label2);
		jframe.add(label3);
		

		jframe.setVisible(true);

		// Thread.sleep(20000);
		//
		// if (g_Input.contains("hi")) {
		// textArea.append("if\n");
		// } else {
		// textArea.append("else\n");
		// }

		// Thread.sleep(5000);
		// textArea.append("hello1\n");
		// Thread.sleep(5000);
		// textArea.append("hello2\n");
		//
		// Thread.sleep(5000);
		// textArea.append("hello3\n");
		//
		// Thread.sleep(5000);
		// textArea.append("hello4\n");
		//

		// JFrame frame = new JFrame();
		// frame.setSize(400, 400);
		// frame.setLayout(null);
		// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//
		//
		// Container container = frame.getContentPane();
		// container.setLayout(new FlowLayout());
		//
		//
		// JTextField textField = new JTextField();
		//
		// textField.setLocation(100,10);
		// textField.setPreferredSize(new Dimension(200, 25));
		// container.add(textField);
		//
		//
		// JButton okButton = new JButton("OK");
		//
		// container.add(okButton);
		// okButton.setVerticalAlignment(SwingConstants.CENTER);
		// //okButton.setVerticalAlignment(SwingConstants.CENTER);
		// okButton.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// String input = textField.getText();
		// System.out.println("Input: " + input);
		// JLabel label = new JLabel("Input will appear here");
		// container.add(label);
		// label.setText(input+"\n");
		//
		// }
		// });
		//
		//
		//
		//
		//
		//
		// JTextArea textArea = new JTextArea(2, 10);
		//
		// //PrintStream printStream = new PrintStream(new
		// CustomOutputStream(textArea));
		// container.add(textArea);
		//
		// textArea.append("hi"+"\n");
		// textArea.append("hello\n");
		// Thread.sleep(10000);
		// textArea.append("added after delay"+"\n");
		//
		// System.out.println("hi");
		//
		//
		//
		// frame.setVisible(true);
	}

	public static void testMethod(String arg) throws InterruptedException {
		try {
			System.out.println("Input 1: " + arg);
			Thread.sleep(10000);

			System.out.println("Input 2: " + arg);
			Thread.sleep(10000);

			System.out.println("Input 3: " + arg);
			Thread.sleep(10000);

			System.out.println("Input 4: " + arg);
			Thread.sleep(10000);

			System.out.println("Input 5: " + arg);
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void printLog() {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					System.out.println("Time now is " + (new Date()));
					try {
						Thread.sleep(1000);
					} catch (InterruptedException ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		thread.start();
	}
	
	public static void writeToFile() throws IOException {
		FileWriter writer = new FileWriter("C:\\phonenumbersearch\\AutomationResults\\testing.csv");
		
		for(int i=0;i<requestid.size();i++) {
			
			writer.append(requestid.get(i));
	        writer.append(',');
	        writer.append(reminder.get(i));
	        writer.append(',');
	        writer.append(status.get(i));
	        writer.append(',');
	        
	        writer.append('\n');	
			
		}
		
        writer.flush();
        
		
	}
	
	
	public static void readFromFile() throws IOException{
		String splitBy = ",";
	    BufferedReader br = new BufferedReader(new FileReader("C:\\phonenumbersearch\\AutomationResults\\testing.csv"));
	    String line = br.readLine();
	    while(line  !=null){
	         String[] b = line.split(splitBy);
	         for(int i=0;i<b.length;i++) {
	        	 System.out.println(b[i]); 
	         }
	         line = br.readLine();
	         
	    }
	    br.close();
	    	
		
	}
	
	

}
