package Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//import org.apache.log4j.Logger;

//import com.sterlingTS.utils.commonUtils.database.DataBaseManager;
//import com.sterlingTS.utils.commonVariables.Globals;

public class AdminClientDAO {
	
//	protected static Logger log = Logger.getLogger(AdminClientDAO.class);
	
	public static DataBaseManager db = null;
	public PreparedStatement ps = 	null;
	public Statement stmt 		=  	null;
	public ResultSet rs 		= 	null;
	public Connection conn;
	
	public static void getAdminDBConnection()
	{
		try{
		String dbURL = "jdbc:sqlserver://qa.absodb.st.com;;portNumber=2410;databaseName=Abshire";
		String dbUserName = "Website";
		String dbPassword = "100%intheDMZ";
		
//		log.info("DB URL is :"+"\t"+dbURL);
		
		db = new DataBaseManager(dbURL,dbUserName,dbPassword);
		}catch(Exception ie)
		{
			ie.printStackTrace();
//			log.info("DB Connection not created properly "+ie.toString());
//			return Globals.KEYWORD_FAIL;
		}
		
//		return Globals.KEYWORD_PASS;
	}
	
	public static void AdminClientDBClose()
	{
		try{
		db.closeConnection();
		}catch(Exception ie){
//			log.info("connection not closed Properly "+ie.toString());
		}
	}
	
	/**************************************************************************************************
	 * Method to fetch the search request count for an order id from from absHire database
	 * @param EmployerName -  EmployerName from Vertex Application - String value
	 * @throws SQLException
	 * @return int ditinct column data
	 * @author navisha
	 * @created 03/26/2018
	 * @LastModifiedBy
	 ***************************************************************************************************/
	public int fetchIntegerColdata(String columnnametobefetched,String searchcriteria) throws SQLException{
		
		int coldata=0;
		String searchReqQuery = "select top 1 * from EmployerMasterDetail where EmployerName=? order by EmployerMasterDetailID desc";
		this.ps = db.conn.prepareStatement(searchReqQuery,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		//ps.setString(1, columnnametobefetched);
		ps.setString(1, searchcriteria);
		rs = ps.executeQuery();
		while(rs.next()) {
			
		coldata =rs.getInt(columnnametobefetched);
		
		}
		return coldata;
	}
	
	/**************************************************************************************************
	 * Method to fetch the search request count for an order id from from absHire database
	 * @param EmployerName -  EmployerName from Vertex Application - String value
	 * @throws SQLException
	 * @return int ditinct column data
	 * @author navisha
	 * @created 03/26/2018
	 * @LastModifiedBy
	 ***************************************************************************************************/
	public String fetchStringColdata(String columnnametobefetched,String searchcriteria) throws SQLException{
		
		String coldata="";
		String searchReqQuery = "select top 1 * from EmployerMasterDetail where EmployerName=? order by EmployerMasterDetailID desc";
		this.ps = db.conn.prepareStatement(searchReqQuery,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		//ps.setString(1, columnnametobefetched);
		ps.setString(1, searchcriteria);
		rs = ps.executeQuery();
	
		while(rs.next())
		{
		 coldata =rs.getString(columnnametobefetched);
		}
		return coldata;
	}
	
	
	/**************************************************************************************************
	 * Method to fetch the employer master details from absHire database
	 * @param EmployerName -  EmployerName from Vertex Application - String value
	 * @throws SQLException
	 * @return int distinct column data
	 * @author navisha
	 * @created 03/26/2018
	 * @LastModifiedBy
	 ***************************************************************************************************/
	public HashMap<String,Object> fetchEmployerDetails(String searchcriteria) throws SQLException{
		
		
		HashMap<String,Object> hmb= new HashMap<>();
		String searchReqQuery = "select top 1 * from EmployerMasterDetail where EmployerName=? order by EmployerMasterDetailID desc";
		this.ps = db.conn.prepareStatement(searchReqQuery,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		
		ps.setString(1, searchcriteria);
		rs = ps.executeQuery();
		while(rs.next()) {
			
			hmb.put("TWNCode", rs.getInt("TWNCode"));
			hmb.put("FulfillmentMethodID",rs.getInt("FulfillmentMethodID"));
			hmb.put("StatedTATHours",rs.getInt("StatedTATHours"));
			hmb.put("Notes",rs.getString("Notes"));
			hmb.put("InternalFee",rs.getString("InternalFee"));
			hmb.put("Address1",rs.getString("Address1"));
			hmb.put("Address2",rs.getString("Address2"));
			hmb.put("City",rs.getString("City"));
			hmb.put("State",rs.getString("State"));
		
		}
		return hmb;
		
	}
	
	
	/**************************************************************************************************
	 * Method to fetch the phone number info from absHire database
	 * @param EmployerName -  EmployerName from Vertex Application - String value
	 * @throws SQLException
	 * @return int distinct column data
	 * @author navisha
	 * @created 03/26/2018
	 * @LastModifiedBy
	 ***************************************************************************************************/
	public boolean fetchEmployerphone(String searchcriteria) throws SQLException{
		
		int count=0;
		boolean flag=false;
		String searchReqQuery = "select * from EmployerMasterDetail em inner join EmployerMasterContacts ec on em.EmployerMasterDetailID=ec.EmployerMasterDetailID where em.EmployerName=?";
		this.ps = db.conn.prepareStatement(searchReqQuery,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ArrayList al= new ArrayList();
		int c;
		String ct,cin,cie,jt,fax,fn,ln;
		ps.setString(1, searchcriteria);
		rs = ps.executeQuery();
		while(rs.next()) {
		
		al.add(new ContactInfo(rs.getInt("ContactOrder"),rs.getString("ContactType"),rs.getString("ContactInfo"),rs.getString("ContactInfoExtn"),rs.getString("JobTitle"),rs.getString("FaxAttentionTo"),rs.getString("FName"),rs.getString("LName")));
		
		}
		
		while(al.size()>0) {
		count++;
		break;
		}
	
		if(count==0) {
			flag=false;
		}
		else {
			flag=true;
			
		}
		return flag;
		 
	}
	/**************************************************************************************************
	 * Method to fetch the Contact info from absHire database
	 * @param EmployerName -  EmployerName from Vertex Application - String value
	 * @throws SQLException
	 * @return int distinct column data
	 * @author navisha
	 * @created 03/26/2018
	 * @LastModifiedBy
	 ***************************************************************************************************/	
	
	
	public void readcontactmap(String searchcriteria) throws SQLException{
		String searchReqQuery = "select * from EmployerMasterDetail em inner join EmployerMasterContacts ec on em.EmployerMasterDetailID=ec.EmployerMasterDetailID where em.EmployerName=?";
		this.ps = db.conn.prepareStatement(searchReqQuery,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ArrayList al= new ArrayList();
//		ContactInfo ci;
		int c;
		String ct,cin,cie,jt,fax,fn,ln;
		ps.setString(1, searchcriteria);
		rs = ps.executeQuery();
		while(rs.next()) {
		
//		ci= new ContactInfo();
//		ci.setfname(rs.getString("FName"));
//		ci.setlname(rs.getString("LName"));
		
		al.add(new ContactInfo(rs.getInt("ContactOrder"),rs.getString("ContactType"),rs.getString("ContactInfo"),rs.getString("ContactInfoExtn"),rs.getString("JobTitle"),rs.getString("FaxAttentionTo"),rs.getString("FName"),rs.getString("LName")));
		}
		for(int i=0;i<al.size();i++) {
			
			c=((ContactInfo) al.get(i)).getcontactorder();
			ct=((ContactInfo) al.get(i)).getcontacttype();
			cin=((ContactInfo) al.get(i)).getcontactinfo();
			cie=((ContactInfo) al.get(i)).contactinfoextn();
			jt=((ContactInfo) al.get(i)).getjobtitle();
			fax=((ContactInfo) al.get(i)).getfaxattentionto();
			fn=((ContactInfo) al.get(i)).getfname();
			ln=((ContactInfo) al.get(i)).getlname();
			
			System.out.println("Row"+i+"is"+"contactorder:"+c+"contacttype:"+ct+"contactinfo:"+cin+"contactinfoextn:"+cie+"Jobtitle:"+jt+"FaxAttention:"+fax+"firstname:"+fn+"lastname:"+ln);
		
		}
	
				
	}

	
	
	

}
