package Test;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;

public class DLLload {

	//Runtime.getRuntime().load("C:\\phonenumbersearch\\SampleDLL.dll");
	public interface SampleDLL extends Library {
		SampleDLL INSTANCE = (SampleDLL) Native.loadLibrary(
            (Platform.isWindows() ? "SampleDLL" : "SampleDLLWindowsPort"), SampleDLL.class);
		
		
        // it's possible to check the platform on which program runs, for example purposes we assume that there's a linux port of the library (it's not attached to the downloadable project)

        public String GetSampleString(String sampleString);
    }
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Runtime.getRuntime().load("C:\\phonenumbersearch\\SampleDLL.dll");
		
		
		SampleDLL sdll = SampleDLL.INSTANCE;
		 
        
 
        String a = "abc";
        String result1 = sdll.GetSampleString(a);  // calling function with int parameter&result
        System.out.println("GetSampleString("+a+"): " + result1);
	}

}
