package Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;





public class S1DB  {

	
	public static void fetchEmployerDetails_FromS1DB(int reqId) throws SQLException, IOException  {
		
		
	
		
		Driver.appendToResultTxtFile("********************************Sterling One Details******************************");
		verifyFieldsInCompanyInformation(reqId);
		String soneDBStatus = Driver.sonedbInfo.get("Status");
		if(soneDBStatus.contains("Employer Phone Number")){
			Driver.allPhnNum.put("S1_DB", "Invalid");
			Driver.appendToResultTxtFile("Employer Name :- "+Driver.sonedbInfo.get("Company Name"));
			Driver.appendToResultTxtFile("Employer City :- "+Driver.sonedbInfo.get("City"));
			Driver.appendToResultTxtFile("Employer State :- "+Driver.sonedbInfo.get("State"));
			Driver.appendToResultTxtFile("Employer Phone Number Status :- Invalid ( "+Driver.sonedbInfo.get("Status")+" )");
			if(Driver.sonedbInfo.get("Contact Number").equalsIgnoreCase("")|| Driver.sonedbInfo.get("Contact Number")==null || Driver.sonedbInfo.get("Contact Number").isEmpty()){
				Driver.appendToResultTxtFile("Employer Phone Number :- Not Provided");
			}else{
				Driver.appendToResultTxtFile("Employer Phone Number :- "+Driver.sonedbInfo.get("Contact Number"));
			}
			Driver.appendToResultTxtFile("");
			System.out.println(Driver.sonedbInfo);
		}else if(soneDBStatus.contains("Valid")){
			
			Driver.appendToResultTxtFile("Employer Name :- "+Driver.sonedbInfo.get("Company Name"));
			Driver.appendToResultTxtFile("Employer City :- "+Driver.sonedbInfo.get("City"));
			Driver.appendToResultTxtFile("Employer State :- "+Driver.sonedbInfo.get("State"));
			Driver.appendToResultTxtFile("Employer Phone Number Status :- Valid ");
			Driver.appendToResultTxtFile("Employer Phone Number :- "+Driver.sonedbInfo.get("Contact Number"));
			Driver.appendToResultTxtFile("");
			Driver.allPhnNum.put("S1_DB", Driver.sonedbInfo.get("Contact Number"));	
			System.out.println(Driver.sonedbInfo);
		}else{
			Driver.allPhnNum.put("S1_DB", "Invalid");
			Driver.appendToResultTxtFile(Driver.sonedbInfo.get("Status"));
			
			//Terminate program
		}
		
		
		
		
	
	}
	
	/**************************************************************************************************
	 * Method to fetch Employer information from S1 database using request id as parameter
	 * @return String - Globals.KEYWORD_PASS if successful, Globals.KEYWORD_FAIL otherwise
	 * @author psave
	 * @throws Exception
	 ***************************************************************************************************/
	public static void verifyFieldsInCompanyInformation(int reqId) throws SQLException{
        String retval="pass";
		String dbURL = "jdbc:mysql://db1-qa.aws.talentwise.com/TalentWise";
		String userName = "readonly";
		String pwd = "soh.Poh9";
		java.sql.Connection db = null;
		ResultSet results = null;
		Statement stmt=null;
		
		try{
			db = DriverManager.getConnection(dbURL,userName,pwd);
			stmt = db.createStatement();
			
			int numRows=0;
			String employerOrgName="",employerPhnNum="", city="",state="";
		
			
			results =stmt.executeQuery("select E.EmployerOrgName,E.ContactTelephone, L.LocationName , L.CityName, L.CountrySubDivisionCode from TalentWise.CandidateEmployment E JOIN OnPrem.RequestQueue R ON E.DetailReportID = R.DetailedReportID JOIN TalentWise.Location L ON E.EmployerLocationID=L.LocationID where R.ReqID="+reqId);
			results.last(); 
			numRows = results.getRow(); 
			results.beforeFirst();
			
			if(numRows!=1){
				if(numRows==0){
					Driver.sonedbInfo.put("Status","Invalid : Unable to Fetch Records for Request Id : "+reqId); 
					System.out.println("Unable to Fetch Records for request Id : "+reqId);
				}else{
					Driver.sonedbInfo.put("Status","Invalid : Getting multiple records for Request Id : "+reqId); 
					System.out.println("Getting multiple records for request Id : "+reqId);
				}
				
			}else{
			
				while (results.next()) {
					employerOrgName = results.getString("EmployerOrgName");
					//employerOrgName = "";
					employerPhnNum = results.getString("ContactTelephone");
					//employerPhnNum = "3333333333";
					city = results.getString("CityName");
					state = results.getString("CountrySubDivisionCode");
					Driver.sonedbInfo.put("Status","Valid"); 
					Driver.sonedbInfo.put("Company Name",employerOrgName);
					Driver.sonedbInfo.put("Contact Number",employerPhnNum);
					Driver.sonedbInfo.put("City",city);
					Driver.sonedbInfo.put("State",state);
        	
					System.out.println(employerOrgName+"   ->"+employerPhnNum+"    ->"+city+"      ->"+state+"  ");
				
				}
				if(employerOrgName.equalsIgnoreCase("")|| employerOrgName==null || employerOrgName.isEmpty()){
					Driver.sonedbInfo.put("Status","Invalid : Employer Name is not found for the request");  
					System.out.println("Employer Name is not found in database for request Id : "+reqId);
				}else if(!employerPhnNum.equalsIgnoreCase("")){
					String formatPhnNo = employerPhnNum.replaceAll("\\D+","").trim();
					if(formatPhnNo.length()< 10){
						Driver.sonedbInfo.put("Status","Employer Phone Number is not 10 digit "); 
						System.out.println("Employer Contact Number is not 10 digit Number for request Id : "+reqId);
					}else if(formatPhnNo.length()>= 10){
						if(formatPhnNo.length()> 10){
							formatPhnNo=formatPhnNo.substring(formatPhnNo.length() - 10);
						}
						
						if(formatPhnNo.charAt(0)=='0' ||formatPhnNo.charAt(0)=='1' || formatPhnNo.charAt(3)=='0' || formatPhnNo.charAt(3)=='1'){
							Driver.sonedbInfo.put("Status","Employer Phone Number :"+employerPhnNum+" is Invalid, 1st or 4th digit of phone Number contain 0 or 1 "); 
							System.out.println("Company Contact Number :"+employerPhnNum+" is Invalid, 1st or 4th digit of phone Number contain 0 or 1  for request Id : "+reqId);
						}else{
							boolean sameNumberflag=true;
							for(char chr: formatPhnNo.toCharArray()){
								Character c=chr;
								if(!c.equals(formatPhnNo.charAt(0))){
									sameNumberflag=false;
									break;
									
								}
								
							}
							if(!sameNumberflag==false){
								Driver.sonedbInfo.put("Status","Employer Phone Number :"+employerPhnNum+" is Invalid, all digits of Number are same"); 
								System.out.println("Company Contact Number :"+employerPhnNum+" is Invalid, all digits of Number are same for request Id : "+reqId);
								
							}
						}
						
					}
						
					
				}else{
					if(Driver.sonedbInfo.get("Contact Number").equalsIgnoreCase("")|| Driver.sonedbInfo.get("Contact Number")==null || Driver.sonedbInfo.get("Contact Number").isEmpty()){
						Driver.sonedbInfo.put("Status","Employer Phone Number"+employerPhnNum+" is not provided"); 
						System.out.println("Company Contact Number"+employerPhnNum+" is not provided for Request Id: "+reqId);
						
					}
				}
				
			}
		
		}catch (SQLException se) {
			se.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			results.close();
			stmt.close();
			db.close();
		}
	
	}
	
}
