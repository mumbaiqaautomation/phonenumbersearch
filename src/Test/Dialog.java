package Test;


import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.commons.lang3.math.NumberUtils;


public class Dialog {

       static String g_Input = "";
       
       public static void GetUserInput() {
           
           JFrame jframe = new JFrame("Verification automation- Phone Number Search");
           jframe.setLayout(null); 
           jframe.setSize(600,600);
           jframe.setLocationRelativeTo(null);//center of the screen
           
           
           JLabel label = new JLabel("Input Request ID:- (for multiple ID's separate it by comma)");
           label.setBounds(100, 20, 400, 30);
           
           JTextField textField = new JTextField();
           textField.setBounds(100, 50, 400, 30);
           
           
           JButton jbutton = new JButton("Submit");  
           jbutton.setBounds(100, 90, 80, 40);
           jbutton.addActionListener(new ActionListener() {
                    @Override
                 public void actionPerformed(ActionEvent e) {
                     String input = textField.getText();
                     validateRequestid(input);
                   
                     System.out.println("Input: " + input);
                     g_Input = input;
                 }
                 });
           
           
           JLabel label1 = new JLabel("Results:-");
           label1.setBounds(100, 180, 400, 20);
           
           JTextArea textArea = new JTextArea();
           textArea.setBounds(100, 200, 300, 250);
           
       
        jframe.add(label);  
           jframe.add(textField);  
           jframe.add(jbutton); 
           jframe.add(label1);
           jframe.add(textArea);
           
           jframe.setVisible(true);
           
           //Thread.sleep(20000);
         
         
                textArea.append("if\n");
                textArea.append("else\n");
         

  }
       
       public static void validateRequestid(String input) {
    	  
    	   ArrayList<String> strob=new ArrayList<String>();;
    	   String[] arr = input.split(",");
    	   if(input.isEmpty()||input.equalsIgnoreCase("")) {
    		   System.out.println("Empty/Null value is entered");
    	   }
    	   
    	   else {
	    	   for(String t:arr) {
	    		   if (t.isEmpty()||t.equalsIgnoreCase("")) {
	    			   System.out.println("Requestid is blank "+t);
	    		   }
	    		   else if(t.length()>=9 && NumberUtils.isDigits(t)) {
	    			   System.out.println("Start execution"); 
	    			   			  
	    			   strob.add(t);
	    		   }
	    		   else if(t.length()<9) {
	    			   System.out.println("Length is less than 9");
	    		   }
	    		   else {
	    			   System.out.println("Wrong input");
	    		   }
	    	   }
	    	   System.out.println("Request ids ready for processing: "+strob);
    	   }  
    	   
       }
       
       public static void main(String[] args) throws Exception  {
    	   GetUserInput();   
             
       	 }

     
       
}


