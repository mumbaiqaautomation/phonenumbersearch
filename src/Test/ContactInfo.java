package Test;


public class ContactInfo {
	
	
	int contactorder;
	String contacttype;
	String contactinfo;
	String contactinfoextn;
	String jobtitle;
	String faxattentionto;
	String fname;
	String lname;
	
	public ContactInfo(int contactorder,String contacttype,String contactinfo,String contactinfoextn,String jobtitle,String faxattentionto,String fname,String lname){
		
		this.contactorder=contactorder;
		this.contacttype=contacttype;
		this.contactinfo=contactinfo;
		this.contactinfoextn=contactinfoextn;
		this.jobtitle=jobtitle;
		this.faxattentionto=faxattentionto;
		this.fname=fname;
		this.lname=lname;
		
	}
	
	public int getcontactorder() {
		return contactorder;
		
	}
	
	public String getcontacttype() {
		return contacttype;
		
	}
	public String getcontactinfo() {
		return contactinfo;
		
	}
	public String contactinfoextn() {
		return contactinfoextn;
		
	}
	public String getjobtitle() {
		return jobtitle;
		
	}
	public String getfaxattentionto() {
		return faxattentionto;
		
	}
	public String getfname() {
		return fname;
		
	}
	public String getlname() {
		return lname;
		
	}
	
	public void setcontactorder(int t1) {
		this.contactorder=t1;
		
	}
	
	public void setcontacttype(String t2) {
		 this.contacttype=t2;
		
	}
	public void setcontactinfo(String t3) {
		 this.contactinfo=t3;
		
	}
	public void setcontactinfoextn(String t4) {
		 this.contactinfoextn=t4;
		
	}
	public void setjobtitle(String t5) {
		 this.jobtitle=t5;
		
	}
	public void setfaxattentionto(String t6) {
		this.faxattentionto=t6;
		
	}
	public void setfname(String t7) {
		this.fname=t7;
		
	}
	public void setlname(String t8) {
		 this.lname=t8;
		
	}
	
	
		
	

}
