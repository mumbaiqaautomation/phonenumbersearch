package Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;



import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;


import com.gargoylesoftware.htmlunit.BrowserVersion;


/**
 * S1 DB > Google > Admin Client DB > Third Party Search  
 * @author Paresh Save(psave), Navisha Mathais(Navisha.Mathais) , Kishan Dave (Kishan.Dave) , Ramesh Tahiliani (Ramesh.Tahiliani)
 *
 */

public class Driver {

	
static HtmlUnitDriver driver;
	
	public static HashMap<String,String> sonedbInfo=new HashMap<String,String>();  
	public static HashMap<String,String> allPhnNum=new HashMap<String,String>(); 
	public static BufferedWriter bw ;
	public static String filePath = "";

	public static void main(String[] args) throws Exception {
		
		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

		
		String employerName = "Road Dog Drivers";
		String employerPhoneNumber = "(469) 320-9575";
		String employerCity ="Dallas";
		String employerState ="TX";
		Boolean isEmployerPhNumberCorrect = true;
		
		
		printCurrentTimeStamp("Execution Started At");
		
		
		int reqId=988963492;
		createTxtFile(Integer.toString(reqId));
		appendToResultTxtFile("Request Id : "+reqId);
		appendToResultTxtFile("");
		
		
		//S1 DB 
		S1DB.fetchEmployerDetails_FromS1DB(reqId);
		
		
		
		
//		initHTMLUnitDriver();
//		
//		
//		
//		
//		
//		//Google
//		if (isEmployerPhNumberCorrect == false) {
//			getNumber_GoogleForwardSearch(employerName, employerCity, employerState); // if new number found log and add to map
//		} else {
//			GoogleRevSearch(employerPhoneNumber,employerName);	//verifies company name is searched against Phone Number 
//		}
//		
//		
//		//Admin Client 
//		AdminClient ac=new AdminClient();
//		ac.fetchdata("Sterling Backcheck Co");
//		
//		
//		//3rd Party Sites
//		YellowPages(employerName,employerCity + ", "+ employerState );
//		Yelp(employerName,employerCity + ", "+ employerState );
//		
//		
//		closeHTMLUnitDriver();
		closeNotepad();
		printCurrentTimeStamp("Execution Ended At");
		
		
	}
	
	
	
	public static void printCurrentTimeStamp(String Message)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(Message + "=>" + dateFormat.format(date));
	}
	
	public static void initHTMLUnitDriver() {
		//*HTMLUnitDriver - Chrome (Need htmlunit-driver-2.29.3.jar and for methods BVdot selenium-server-standalone-3.9.1.jar)
		
		//System.setProperty("webdriver.chrome.driver", "C:\\Users\\rtahiliani\\eclipse_workspace\\chromedriver_old.exe");
		// driver = new ChromeDriver();
		driver = new HtmlUnitDriver(BrowserVersion.INTERNET_EXPLORER);	
		//driver.setJavascriptEnabled(true);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
    }
	
	public static void closeHTMLUnitDriver() {
		driver.close();
		driver.quit();
	}
	
	

	
	/**
	 * opens google > enters phone number > check if complete employer name is matched in any of the links displayed on 1st page
	 * if found : returns the url 
	 * if not found : loggs that it is not found on 1st page 
	 * @param employerName : input from S1 DB function
	 * @param  employerPhoneNumber : input from S1 DB function 
	 * @return status_linkFoundonGooglePage1 : if link found on page1 then the status will be success else un-success
	 */
	public static String GoogleRevSearch(String employerPhoneNumber, String employerName) {
	
		//Open Google and perfoem Reverse Search 
		driver.setJavascriptEnabled(true);
		driver.get("http://www.google.com");
		System.out.println(driver.getTitle());
		printCurrentTimeStamp("Page Loaded At");
		
		driver.findElement(By.name("q")).sendKeys(employerPhoneNumber);
		driver.findElement(By.name("q")).submit();
		
		List<WebElement> searchLinks = driver.findElements(By.xpath("//h3[@class='r']/a")); 		
		System.out.println("Total number of links on 1st page are : "+searchLinks.size());
		
		String status_linkFoundonGooglePage1 =  "un-success";
		for (int i = 0; i < searchLinks.size(); i++) {
			WebElement wel = searchLinks.get(i);
			System.out.println(wel.getText());	
			if (wel.getText().contains(employerName)) {
				System.out.println("************** For Phone number '"+employerPhoneNumber+"' ,Full Employer Name '"+employerName+"' Found on link Header '"+wel.getText()+ "'. It's URL is: "+wel.getAttribute("href"));
				status_linkFoundonGooglePage1 =  "success";
				break;
			}if(i==searchLinks.size()-1) {
				System.out.println("************** For Phone number '"+employerPhoneNumber+"' ,Full Employer Name '"+employerName+"' NOT Found on Google Page1" );
			}
			
		}
		
		return status_linkFoundonGooglePage1;
	}
	
	/**
	 * opens google > enters EmployerName+City+State  > check header is displayed with same Employer and get the contact number from header 
	 * if found : logs the number and adds it to map 
	 * if not found : loggs that it is not found on header  
	 * @param employerName : input from S1 DB function
	 * @param  employerCity : input from S1 DB function 
	 * @param  employerState : input from S1 DB function
	 */
	public static void getNumber_GoogleForwardSearch(String employerName, String employerCity, String employerState ) throws Exception {
		
		//Open Google and perform forward Search 
		driver.setJavascriptEnabled(true);
		driver.get("http://www.google.com");
		System.out.println(driver.getTitle());
		printCurrentTimeStamp("Page Loaded At");
		
		
		System.out.println("Keyword used to serach on google :- "+employerName + " "+ employerCity + " "+ employerState + " Contact Number");
		driver.findElement(By.name("q")).sendKeys(employerName + " "+ employerCity + " "+ employerState + " Contact Number");
		driver.findElement(By.name("q")).submit();
		Thread.sleep(1000);
		System.out.println("page title is :-"+driver.getTitle());
	
		
		
		List<WebElement> googleCompanyNameObject = 	driver.findElements(By.xpath("//div[@class='kp-header']"));
		System.out.println("# of Header found "+googleCompanyNameObject.size());
		if (googleCompanyNameObject.size()>0) {
			String headerOutput = googleCompanyNameObject.get(0).getText();
			System.out.println("Complete header outut is :-"+ headerOutput);
			
			if (headerOutput.split("\n")[1].toLowerCase().contains(employerName.toLowerCase())) {
				String googleEmployerNumber = headerOutput.split("\n")[0];
				System.out.println("***********Google forward search phone number is :-"+googleEmployerNumber);
			} else {
				System.out.println("Employer Name did not match in the header");		
			};
			
			
		} else {

		}
		
	
		
	}
	
	
	 public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{

	        //Convert web driver object to TakeScreenshot

	        TakesScreenshot scrShot =((TakesScreenshot)webdriver);

	        //Call getScreenshotAs method to create image file

	                File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

	            //Move image file to new destination

	                File DestFile=new File(fileWithPath);

	                //Copy file at destination

	                FileUtils.copyFile(SrcFile, DestFile);

	    }
	
	 
	 
	 //--------YELLOW PAGES---------------------
	 public static void YellowPages(String EmployerName,String Loc) {
		 driver.setJavascriptEnabled(false);
		 System.out.println("yellowpages start"+ EmployerName +Loc );
         driver.get("https://www.yellowpages.com/");
         System.out.println(driver.getTitle());
         driver.findElement(By.name("geo_location_terms")).clear();
         driver.findElement(By.name("geo_location_terms")).sendKeys(Loc);
         driver.findElement(By.name("search_terms")).sendKeys(EmployerName);
         driver.findElement(By.name("search_terms")).submit();
         List<WebElement> links = driver.findElements(By.xpath("//div[@class='v-card']"));           
         System.out.println(links.size());
         
         for (int i = 0; i < links.size(); i++) {
        	 	String CompanyName = "",Phoneno="",Address="";
                WebElement wel = links.get(i);
                try{
	                 CompanyName = wel.findElement(By.xpath(".//a[@class='business-name']")).getText();
	                 Phoneno = wel.findElement(By.xpath(".//div[@class='phones phone primary']")).getText();
	                 Address = wel.findElement(By.xpath(".//p[@class='adr']")).getText();
                }
                catch( NoSuchElementException ignored){
	                System.out.println("Required Details not fetched from yellow pages");
	                System.out.println("could just fetch :-"+CompanyName+" "+Address+" "+Phoneno);
                }
                if(EmployerName.toLowerCase().equalsIgnoreCase(CompanyName.toLowerCase()) && Address.toLowerCase().contains(Loc.toLowerCase())){
                	System.out.println(CompanyName+" "+Address+" "+Phoneno);      
                }
                System.out.println("-------");

         }
         
	 }
       
       //--------Yelp---------------------
	 public static void Yelp(String EmployerName,String Loc) {
		 driver.setJavascriptEnabled(false);
		 System.out.println("YELP start"+ EmployerName +Loc );
		 driver.get("https://www.yelp.com/");
         System.out.println(driver.getTitle());
 
         driver.findElement(By.id("dropperText_Mast")).clear();
         driver.findElement(By.id("dropperText_Mast")).sendKeys(Loc);
         driver.findElement(By.id("find_desc")).sendKeys(EmployerName);
         driver.findElement(By.id("find_desc")).submit();

        List<WebElement> links = driver.findElements(By.xpath("//div[@class='biz-listing-large']"));           
         System.out.println(links.size());
         
         for (int i = 0; i < links.size(); i++) {
           String CompanyName = "",Phoneno="",Address="";
                WebElement wel = links.get(i);
                try{
                     CompanyName = wel.findElement(By.xpath(".//a[@class='biz-name js-analytics-click']")).getText().replace("�", "'");
                     Phoneno = wel.findElement(By.xpath(".//span[@class='biz-phone']")).getText();
                     Address = wel.findElement(By.xpath(".//address")).getText().replace("\n", "");
                }
                catch( NoSuchElementException ignored){
                    System.out.println("Required Details not fetched from yellow pages");
                    System.out.println(CompanyName+" "+Address+" "+Phoneno);
                }
                    if(EmployerName.toLowerCase().equalsIgnoreCase(CompanyName.toLowerCase()) && Address.toLowerCase().contains(Loc.toLowerCase())){
                    System.out.println(CompanyName+" "+Address+" "+Phoneno);      
                }
                System.out.println("-------");

        }


	 }
	 
 	/**************************************************************************************************
	 * Method to Create the folder for store the results
	 * @return String - Globals.KEYWORD_PASS if successful, Globals.KEYWORD_FAIL otherwise
	 * @author psave
	 * @throws Exception
	 ***************************************************************************************************/
	public static void mkDirs(String dirStructurePath){
		File abc = new File(dirStructurePath);
    	abc.mkdirs();
	}
	/**************************************************************************************************
	 * Method to Create the folder for store the results
	 * @return String - Globals.KEYWORD_PASS if successful, Globals.KEYWORD_FAIL otherwise
	 * @author psave
	 * @throws IOException 
	 * @throws Exception
	 ***************************************************************************************************/
	public static void createTxtFile(String reqID) throws IOException{
		 String TestDir = System.getProperty("user.dir");
		 System.out.println("Test Dir path -"+TestDir);
		 Date date = new Date();  
		 SimpleDateFormat formatter = new SimpleDateFormat("dd_MMMM_yyyy");  
		 String strDate = formatter.format(date);  
		 String cuurentFilePath=TestDir+File.separator+"AutomationResults"+File.separator+strDate;
		 mkDirs(cuurentFilePath);
		 
		 String fileName=reqID+".txt";
		 filePath=cuurentFilePath+File.separator+fileName;
		 boolean fileExist=false;
		 int i=1;
		 int fileCreation=1;
		 while(fileExist==false && fileCreation<=20){
			filePath=cuurentFilePath+File.separator+fileName;
			if ( new File(filePath).createNewFile()){
				System.out.println("File is created!");
				fileExist=true;
			}else{
				
				System.out.println("File already exists.");
				fileName=reqID+"_"+i+".txt";
				i++;
				fileCreation++;
			}
		 }
		 // APPEND MODE SET HERE
		 bw = new BufferedWriter(new FileWriter(filePath, true));
//		if(fileExist==true){
//			return FilePath;
//		}else{
//			return "File not Created";
//		}
	}
	/**************************************************************************************************
	 * Method to append results in text file
	 * @return String - Globals.KEYWORD_PASS if successful, Globals.KEYWORD_FAIL otherwise
	 * @author psave
	 * @throws IOException 
	 * @throws Exception
	 ***************************************************************************************************/
		public static void appendToResultTxtFile (String txt) {

	  	      try {
	         bw.newLine();
			 bw.write(txt);
	      }
	      catch (IOException ioe2) {
	    	  
	      }
	
		}
	
		public static void closeNotepad()
		{
			try {
			 bw.flush();
		      } catch (IOException ioe) {
			 ioe.printStackTrace();
		      } finally {                       // always close the file
			 if (bw != null) try {
			    bw.close();
			 } catch (IOException ioe2) {
			    // just ignore it
			 }
		      } // end try/catch/finally

		}
	 
}