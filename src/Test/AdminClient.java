package Test;

import java.sql.SQLException;

public class AdminClient {

	

	AdminClientDAO obj=new AdminClientDAO ();
	
	
	public  void fetchdata(String employername) throws SQLException {
		
		String notes="";
		String statedtathours="";
		String address1="";
		String address2="";
		String city="";
		String state="";
		String internalFee="";
		int fulfillmentid=0;
		boolean emp_present=false;
		boolean contactdetails=false;
//		boolean fulfill_valid=false;
		
		AdminClientDAO.getAdminDBConnection();
		emp_present=this.checkifEmployernameExists(employername);
		
		
		if(emp_present==true ) {
			contactdetails=obj.fetchEmployerphone(employername);
			
			if(this.checkifFulfillmentvalid(employername)==true) {
				
				fulfillmentid=obj.fetchIntegerColdata("FulfillmentMethodID",employername);
				
				//fetch details on the basis of fulfillmentid
				switch(fulfillmentid)
				{
				case 1: //Email
					 notes = obj.fetchEmployerDetails(employername).get("Notes").toString();
					 statedtathours=obj.fetchEmployerDetails(employername).get("StatedTATHours").toString();
					 System.out.println("Notes:"+notes+"statedtathours:"+statedtathours);
					 if(contactdetails==true) {
					 obj.readcontactmap(employername);
					 }
					 else {
						 System.out.println("No contact details found");
					 }
					 break;
				
				
				case 2: //Fax
					 notes = obj.fetchEmployerDetails(employername).get("Notes").toString();
					 statedtathours=obj.fetchEmployerDetails(employername).get("StatedTATHours").toString();
					 System.out.println("Notes:"+notes+"statedtathours:"+statedtathours);
					 if(contactdetails==true) {
						 obj.readcontactmap(employername);
						 }
						 else {
							 System.out.println("No contact details found");
						 }
					 break;
				
				
				case 3: //Phone
					 notes = obj.fetchEmployerDetails(employername).get("Notes").toString();
					 statedtathours=obj.fetchEmployerDetails(employername).get("StatedTATHours").toString();
					 System.out.println("Notes:"+notes+"statedtathours:"+statedtathours);
					 if(contactdetails==true) {
						 obj.readcontactmap(employername);
						 }
						 else {
							 System.out.println("No contact details found");
						 }
					 break;
				
				
				case 4: //TWN
					 notes = obj.fetchEmployerDetails(employername).get("Notes").toString();
					 statedtathours=obj.fetchEmployerDetails(employername).get("TWNCode").toString();
					 System.out.println("Notes:"+notes+"statedtathours:"+statedtathours);
					 if(contactdetails==true) {
						 obj.readcontactmap(employername);
						 }
						 else {
							 System.out.println("No contact details found");
						 }
					 break;
				
				
				case 5: //Verifyjob
					
					break;
				
				
				case 6: //PostalMail
					 notes = obj.fetchEmployerDetails(employername).get("Notes").toString();
					 statedtathours=obj.fetchEmployerDetails(employername).get("StatedTATHours").toString();
					 address1=obj.fetchEmployerDetails(employername).get("Address1").toString();
					 address2=obj.fetchEmployerDetails(employername).get("Address2").toString();
					 city=obj.fetchEmployerDetails(employername).get("City").toString();
					 state=obj.fetchEmployerDetails(employername).get("State").toString();
					 internalFee=obj.fetchEmployerDetails(employername).get("InternalFee").toString();
					 System.out.println("Notes:"+notes+"statedtathours:"+statedtathours+"address1:"+address1+"address2:"+address2+"city:"+city+"state:"+state+"internalFee:"+internalFee);
					 break;
				
				
				case 7: //SeeNotes
					 notes = obj.fetchEmployerDetails(employername).get("Notes").toString();
					 internalFee=obj.fetchEmployerDetails(employername).get("InternalFee").toString();
					 if(contactdetails==true) {
						 obj.readcontactmap(employername);
						 }
						 else {
							 System.out.println("No contact details found");
						 }
					 System.out.println("Notes:"+notes+"internalFee:"+internalFee);
					 break;
				
				
				case 8: //Online
					 
					break;
				
				
				case 9: //other vendor
					 notes = obj.fetchEmployerDetails(employername).get("Notes").toString();
					 internalFee=obj.fetchEmployerDetails(employername).get("InternalFee").toString();
					 if(contactdetails==true) {
						 obj.readcontactmap(employername);
						 }
						 else {
							 System.out.println("No contact details found");
						 }
					 System.out.println("Notes:"+notes+"internalFee:"+internalFee);
					break;
					
				default:
					System.out.println("Invalid");
			}
				
				
			}
			else {
				//fulfillment is invalid but employer exists
				
				 notes = obj.fetchEmployerDetails(employername).get("Notes").toString();
				 statedtathours=obj.fetchEmployerDetails(employername).get("StatedTATHours").toString();
				 address1=obj.fetchEmployerDetails(employername).get("Address1").toString();
				 address2=obj.fetchEmployerDetails(employername).get("Address2").toString();
				 city=obj.fetchEmployerDetails(employername).get("City").toString();
				 state=obj.fetchEmployerDetails(employername).get("State").toString();
				 internalFee=obj.fetchEmployerDetails(employername).get("InternalFee").toString();
				 System.out.println("Notes:"+notes+"statedtathours:"+statedtathours+"address1:"+address1+"address2:"+address2+"city:"+city+"state:"+state+"internalFee:"+internalFee);
				 if(contactdetails==true) {
					 obj.readcontactmap(employername);
					 }
					 else {
						 System.out.println("No contact details found");
					 }
			}
			
			
		}
		
		else {
			//employer does not exist
			System.out.println("Employer not found");
		}
		
		
	}
	/**************************************************************************************************
	 * Method to check if employer details exist in adminclient db
	 * @param EmployerName -  EmployerName from Vertex Application - String value
	 * @throws SQLException
	 * @return boolean true if exist and false otherwise
	 * @author navisha
	 * @created 03/29/2018
	 * @LastModifiedBy
	 ***************************************************************************************************/	
	
	
	public  boolean checkifEmployernameExists(String employername) throws SQLException{
		String coldata;
		coldata = obj.fetchStringColdata("EmployerName",employername);
		
		if(coldata.isEmpty()||coldata.equalsIgnoreCase("")) {
			return false;
		}
		else {
			return true;
		}
		
	}
	
	/**************************************************************************************************
	 * Method to check if fulfillment status details are valid
	 * @param EmployerName -  EmployerName from Vertex Application - String value
	 * @throws SQLException
	 * @return boolean true if valid and false otherwise
	 * @author navisha
	 * @created 03/29/2018
	 * @LastModifiedBy
	 ***************************************************************************************************/	
	
	
	public boolean checkifFulfillmentvalid(String employername) throws SQLException{
		int coldata;
		coldata = obj.fetchIntegerColdata("FulfillmentMethodID",employername);
		
		if(coldata==0) {
			return false;
		}
		else {
			return true;
		}
		
	}
	
	
	
}
